package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func uploadFile(w http.ResponseWriter, r *http.Request) {
	fmt.Println("File Upload Endpoint Hit")

	r.ParseMultipartForm(10 << 20)
	fastno := r.FormValue("fastno")
	posid := r.FormValue("psno")
	addrnum := r.FormValue("addrprfno")
	rcnum := r.FormValue("rcno")
	topup := r.FormValue("topup")
	var data = [][]string{{"FastTagId", fastno}, {"point of sale ID", posid}, {"Address proof number", addrnum}, {"RC number", rcnum}, {"TopUp amount", topup}}

	path := "/projects/src/restapi/" + fastno
	os.MkdirAll(path, 0711)
	csvname := "/projects/src/restapi/" + fastno + "/" + fastno + ".csv"
	csvfile, err := os.Create(csvname)
	if err != nil {
		fmt.Println("error creating csv file")
		fmt.Println(err)
	}
	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)
	defer writer.Flush()
	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			fmt.Println("error while writing to csv file")
			fmt.Println(err)
		}
	}

	//Retrieving address proof
	addrprf, _, err := r.FormFile("addrprf")
	if err != nil {
		fmt.Println("error while retrieving form address file")
		fmt.Println(err)
	}
	if addrprf != nil {
		addrFile, err := ioutil.TempFile(fastno, "upload-addrprf*.png")
		if err != nil {
			fmt.Println("Error Retrieving the File")
			fmt.Println(err)
			return
		}
		addrBytes, err := ioutil.ReadAll(addrprf)
		if err != nil {
			fmt.Println("error while reading from address file")
			fmt.Println(err)
		}
		addrFile.Write(addrBytes)
	}
	//Retrieving Application form
	applfrm, _, err := r.FormFile("applfrm")
	if err != nil {
		fmt.Println("error while retrieving form application file")
		fmt.Println(err)
	}
	if applfrm != nil {
		applFile, err := ioutil.TempFile(fastno, "upload-applfrm-*.png")
		if err != nil {
			fmt.Println(err)
		}

		applBytes, err := ioutil.ReadAll(applfrm)
		if err != nil {
			fmt.Println(err)
		}
		applFile.Write(applBytes)
	}
	//Retrieving RC image
	rcimg, _, err := r.FormFile("rcimg")
	if err != nil {
		fmt.Println("error while retrieving form rc file")
		fmt.Println(err)
	}
	if rcimg != nil {
		rcFile, err := ioutil.TempFile(fastno, "upload-RC-*.png")
		if err != nil {
			fmt.Println(err)
		}

		rcBytes, err := ioutil.ReadAll(rcimg)
		if err != nil {
			fmt.Println(err)
		}
		rcFile.Write(rcBytes)
	}
	//Retrieving pancard image
	panimg, _, err := r.FormFile("panimg")
	if err != nil {
		fmt.Println("error while retrieving form pancard file")
		fmt.Println(err)
	}
	if panimg != nil {
		panFile, err := ioutil.TempFile(fastno, "upload-pancard-*.png")
		if err != nil {
			fmt.Println(err)
		}

		panBytes, err := ioutil.ReadAll(panimg)
		if err != nil {
			fmt.Println(err)
		}
		panFile.Write(panBytes)
	}
	//Retrieving vehicle image
	vehimg, _, err := r.FormFile("vehimg")
	if err != nil {
		fmt.Println("error while retrieving form vehicleimg file")
		fmt.Println(err)
	}
	if vehimg != nil {
		vehFile, err := ioutil.TempFile(fastno, "upload-vehimage-*.png")
		if err != nil {
			fmt.Println(err)
		}

		vehBytes, err := ioutil.ReadAll(vehimg)
		if err != nil {
			fmt.Println(err)
		}
		vehFile.Write(vehBytes)
	}
	//Retrieving other documents
	othdoc, _, err := r.FormFile("othdoc")
	if err != nil {
		fmt.Println("error while retrieving form other documents file")
		fmt.Println(err)
	}
	if othdoc != nil {
		othFile, err := ioutil.TempFile(fastno, "upload-othdoc-*.png")
		if err != nil {
			fmt.Println(err)
		}
		othBytes, err := ioutil.ReadAll(othdoc)
		if err != nil {
			fmt.Println(err)
		}
		othFile.Write(othBytes)
	}
	defer addrprf.Close()
	defer applfrm.Close()
	defer rcimg.Close()
	defer panimg.Close()
	defer othdoc.Close()
	defer vehimg.Close()

	fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func setupRoutes() {
	http.HandleFunc("/upload", uploadFile)
	http.ListenAndServe(":8080", nil)
}

func main() {
	fmt.Println("Hello World")
	setupRoutes()
}
